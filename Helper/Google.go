package Helper

import (
    
    "encoding/json"
    "log"
    "io/ioutil"
    "fmt"
    "net/http"
    )


type Ginfo struct {
	
    Sub string `json:"sub"`
    Name string `json:"name"`
    GivenName string `json:"given_name"`
    FamilyName string `json:"family_name"`
    Profile string `json:"profile"`
    Picture string `json:"picture"`
    Email string `json:"email"`
    EmailVerified string `json:"email_verified"`
    Gender string `json:"gender"`

}

func GoogleTokenValidate(tokenid string)(string){
    var c Ginfo
    res, err := http.Get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="+tokenid)
    if err != nil {
        log.Fatal(err)
        //return err
    }

    fmt.Println(res.Body)
    certs, err := ioutil.ReadAll(res.Body)
    log.Println("Email body: ", string(certs))
    res.Body.Close()
    if err != nil {
        log.Fatal(err)
        //return err
    }
    err = json.Unmarshal(certs, &c)
    if err != nil {
        log.Fatal(err)
        //return err
    }
    return c.Email
}