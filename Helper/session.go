package Helper
import (
	"github.com/gorilla/securecookie"
	"net/http"
	)

var cookieHandler = securecookie.New(
  					securecookie.GenerateRandomKey(64),
     				securecookie.GenerateRandomKey(32))

func SetSession(userName string, response http.ResponseWriter) {
     value := map[string]string{
         "userid": userName,
     }
     if encoded, err := cookieHandler.Encode("session", value); err == nil {
         cookie := &http.Cookie{
             Name:  "session",
             Value: encoded,
             Path:  "/",
         }
         http.SetCookie(response, cookie)
     }
 }
 
 func GetUserID(request *http.Request) (userName string) {
     if cookie, err := request.Cookie("session"); err == nil {
         cookieValue := make(map[string]string)
         if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
             userName = cookieValue["userid"]
         }
     }
     return userName
 }
 
 func ClearSession(response http.ResponseWriter) {
     cookie := &http.Cookie{
         Name:   "session",
         Value:  "",
         Path:   "/",
         MaxAge: -1,
     }
     http.SetCookie(response, cookie)
}