package DBModel

import (
    _ "github.com/go-sql-driver/mysql"
    "database/sql"
    "fmt"
    "log"
)
var SqlConnString = "root:masterkey@tcp(localhost:3306)/login_sys_db"
func DBInsertUserAccount(username string, password string , signup_type string) int64 {
	db, err := sql.Open("mysql",SqlConnString)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	err = db.Ping()
	if err != nil {
		fmt.Println(err)
	}
	sqlStr := "INSERT INTO user_accounts(username,password,type) VALUES(?, PASSWORD(?) , ?)"
    stmt, err := db.Prepare(sqlStr)
    res, _ := stmt.Exec(username, password, signup_type)
    if err != nil {
        // do something here
        fmt.Println(err)
    }
    affect, err := res.RowsAffected()
    if err != nil {
        // do something here
        fmt.Println(err)
    }

    id, _ := res.LastInsertId()
    fmt.Println("last insertedID :" ,id)
    fmt.Println(affect)
    return id

}


func DBUpdateUserAccount(userid string , fullname string , address string , email string, telno string) int64 {
    db, err := sql.Open("mysql", SqlConnString)
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()
    err = db.Ping()
    if err != nil {
        fmt.Println(err)
    }
    sqlStr := "update user_accounts set username = ? , fullname= ? ,address = ?,  telephone = ? where userid = ?"
    stmt, err := db.Prepare(sqlStr)
    fmt.Println("insert duplicate data : " , err)
    res, _ := stmt.Exec(email,fullname,address,telno,userid)
    if err != nil {
        // do something here
        fmt.Println(err)
    }
    affect, err := res.RowsAffected()
    if err != nil {
        // do something here
        fmt.Println(err)
    }

    fmt.Println(affect)
    return affect

}


func DBUpdateUserAccountPAssword(email string,password string ) int64 {
    db, err := sql.Open("mysql", SqlConnString)
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()
    err = db.Ping()
    if err != nil {
        fmt.Println(err)
    }
    sqlStr := "update user_accounts set password= PASSWORD(?) , type= ? where username = ?"
    stmt, err := db.Prepare(sqlStr)
    fmt.Println("insert duplicate data : " , err)  //for debugging
    res, _ := stmt.Exec(password,`manual`,email)
    if err != nil {
        // do something here
        fmt.Println(err)
    }
    affect, err := res.RowsAffected()
    if err != nil {
        // do something here
        fmt.Println(err)
    }

    fmt.Println(affect)
    return affect

}

func DBGetUserAccount(userID string) []string {
    sqldata := Data_row(`select * from user_accounts where userid =`+ userID)
    return sqldata
}
func DBGetUserAccountbyEmailAndPassword(email string, password string , logintype string) int64 {
    db, err := sql.Open("mysql", SqlConnString)
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()
    err = db.Ping()
    if err != nil {
        fmt.Println(err)
    }
    sqlStr := "select userid from user_accounts where username=? and password=PASSWORD(?) and type= ?"
    stmt, err := db.Prepare(sqlStr)
    var userid int64
    err = stmt.QueryRow(email,password,logintype).Scan(&userid)
    if err != nil {
        // do something here
        fmt.Println(err)
    }
    return userid
}

func ValidatEmail(email string, userid string )[]string {
    sqldata := Data_row(`select username from user_accounts where username ='`+ email+`' and userid<>`+userid)
    fmt.Println(sqldata)
    return sqldata

}