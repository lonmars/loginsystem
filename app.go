package main

import (
    _ "github.com/go-sql-driver/mysql"
    "fmt"
    "net/http"
    "text/template"
    "strconv"
    "encoding/json"
    DBModel "./DBModel"
    Helper "./Helper"

)
//encryption key
var Encrypt_key = []byte(`Encrypt_key_mu5e5ecreT2017_@!234`) 

func main() {

	mux := http.NewServeMux()
	
	mux.HandleFunc("/" , IndexHandler)
	mux.HandleFunc("/dologin" , DoSigninHandler)
	mux.HandleFunc("/signup" , SignUpHandler)
	mux.HandleFunc("/forgotpassword" , ForgotPasswordHandler)
	mux.HandleFunc("/passwordreset" , PAsswordResetHandler)
	mux.HandleFunc("/DoSignup" , DoSignUpHandler)
	mux.HandleFunc("/FillProfileInfoHandler" , FillProfileInfoHandler)
	mux.HandleFunc("/ProfilePageHandler" , ProfilePageHandler)
	mux.HandleFunc("/EditProfilePageHandler" , EditProfilePageHandler)
	mux.HandleFunc("/validate_email" , ValidateEmailHandler)

	mux.HandleFunc("/logout" , LogoutHandler)


	// file server for serving js, css and html
	fs := http.FileServer(http.Dir("/Go/Login_system/resources"))
	mux.Handle("/static/", http.StripPrefix("/static/", fs))

	//port initialization
	str_port := ":31"  
	http.ListenAndServe(str_port,mux) // listen on all interfaces on port 31

}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	tconf := make(map[string]string)
	tconf["flag"] =  r.URL.Query().Get("flag")	
	tmpl := template.New("index.html")
	var err error
	if tmpl, err = tmpl.ParseFiles("resources/html/index.html"); err != nil {
		fmt.Println(err)
	}
	err = tmpl.Execute(w,tconf)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

type Return_info struct{
	Userid string
	Email string
	Message string
}

func DoSigninHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method =="POST" {
		r.ParseForm()
		var email , password string
		auth_type := r.Form["type"][0]

		if auth_type == `manual`{
			
			email 		= r.Form["email"][0]
			password 	= r.Form["password"][0]
			//database call check if the username and password exist
			int_userID64 := DBModel.DBGetUserAccountbyEmailAndPassword(email,password,`manual`)
			
			if int_userID64 > 0 {
				//conert int64 to string
				str_userID := strconv.FormatInt(int_userID64, 10)
				Helper.SetSession(str_userID, w)
 		        redirectURL := "/ProfilePageHandler?userid="+str_userID//+userid
				http.Redirect(w, r, redirectURL , 302)	
			}else{
				redirectURL := "?flag=1"
				http.Redirect(w, r, redirectURL , 302)	
			}

		}else{
			// signup via google api
			tokenid 	:= r.Form["tokenid"][0]
			email = Helper.GoogleTokenValidate(tokenid)
		    password = ``
		    
		    
			var retData Return_info
		    // database check if member is exist
		    int_userID64 := DBModel.DBGetUserAccountbyEmailAndPassword(email,password,auth_type)
			
			if int_userID64 > 0 {
				//conert int64 to string
				str_userID := strconv.FormatInt(int_userID64, 10)
				Helper.SetSession(str_userID, w)
				retData = Return_info{str_userID,email,`Success`}
			}else{
				retData = Return_info{``,email,`Email not register or manually register. Please Login via manual or do Signup`}
			}

			js, err := json.Marshal(retData)
			if err != nil {
		    	http.Error(w, err.Error(), http.StatusInternalServerError)
		    	return
	  		}
			w.Header().Set("Content-Type", "application/json")
		  	w.Write(js)  
		}
		
		
	}	
}


func SignUpHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method =="GET" {
		tmpl := template.New("signup.html")
		var err error
		if tmpl, err = tmpl.ParseFiles("resources/html/signup.html"); err != nil {
			fmt.Println(err)
		}
		err = tmpl.Execute(w,nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}
	}
}

func ForgotPasswordHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method =="GET" {
		tconf := make(map[string]string)
		tconf["flag"] =  r.URL.Query().Get("flag")
		tmpl := template.New("forgot_password.html")
		var err error
		if tmpl, err = tmpl.ParseFiles("resources/html/forgot_password.html"); err != nil {
			fmt.Println(err)
		}
		err = tmpl.Execute(w,tconf)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}
	}else if r.Method =="POST"{
		r.ParseForm()
		email 	:= r.Form["email"][0]
		// Encrypt email data to make it as token
		cryp_email :=Helper.Encrypt(Encrypt_key,email)	
		fmt.Println(cryp_email)
		tconf := make(map[string]string)
		mymailpassword := Helper.Decrypt(Encrypt_key,`aWHr1wb_6Lb12ZOq3hnal1sYIHxwmcwvDA==`) // to hide may password
		hkey := cryp_email
		err:=Helper.SendEmail(
        "smtp.gmail.com",
        587,
        "marlonpamisa@gmail.com",
        mymailpassword,
        []string{email},
        "Password recovery",
        `

			Opps it seems like you forgot your password.  to recover kindly click this link http://`+r.Host+`/passwordreset?token=`+hkey+`
			<br>If you have problems, please paste the above URL into your web browser.
			<br>
			<br>
			P.S.: This is an auto-generated message. <br>Please do not reply to this email.
			<br>
			<br>
			Thanks,
			<br>Applicant developer

        `,
        tconf)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}else{
			redirectURL := "/forgotpassword?flag=1"
			http.Redirect(w, r, redirectURL , 302)	
		}
	}

}

func PAsswordResetHandler(w http.ResponseWriter, r *http.Request) {
	
	if r.Method =="GET" {
		tconf := make(map[string]string)
		
		tconf["flag"] =  r.URL.Query().Get("flag")	// success handling feature, 1 if update success 0 for not 
		tconf["email"] =  ``
		if( tconf["flag"]!=`1`){
			//Decrypt token 
			Dec_email :=Helper.Decrypt(Encrypt_key,r.URL.Query().Get("token"))
			tconf["email"] =  Dec_email
		}
		tmpl := template.New("password_reset.html")
		var err error
		if tmpl, err = tmpl.ParseFiles("resources/html/password_reset.html"); err != nil {
			fmt.Println(err)
		}
		err = tmpl.Execute(w,tconf)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}
	}else if r.Method =="POST"{
		r.ParseForm()
		password 	:= r.Form["password"][0]
		email 		:= r.Form["email"][0]

		// update record to database
		DBModel.DBUpdateUserAccountPAssword(email,password)
		redirectURL := "/passwordreset?flag=1"
		http.Redirect(w, r, redirectURL , 302)	
		
	}

}



func DoSignUpHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method =="POST" {
		r.ParseForm()
		fmt.Println(r.ParseForm())
		auth_type := r.Form["type"][0]
		var email, password string
		if auth_type == `manual`{
			email 		= r.Form["email"][0]
			password 	= r.Form["password"][0]

			userID := DBModel.DBInsertUserAccount(email,password,auth_type)
			if userID > 0 { // if user greater than 0,it means login success
				str_userID := strconv.FormatInt(userID, 10)
				redirectURL := "/FillProfileInfoHandler?userid="+str_userID+"&email="+email
				http.Redirect(w, r, redirectURL , 302)	
			}	
		}else{
			// signup via google api
			tokenid 	:= r.Form["tokenid"][0]
			email = Helper.GoogleTokenValidate(tokenid)
		    password = ``
		    
		    var retData Return_info
		    //validate email if aleady exist
		    if(len(DBModel.ValidatEmail(email,`0`)) ==0){
				
				userID := DBModel.DBInsertUserAccount(email,password,auth_type)
			    if userID > 0 { // if user greater than 0,it means login success
			    	str_userID := strconv.FormatInt(userID, 10)
					retData = Return_info{str_userID,email,`Success`}
					
				}
				
			}else{
				retData = Return_info{``,``,`Email already Exist, Try another`}
				
			}
			js, err := json.Marshal(retData)
			if err != nil {
		    	http.Error(w, err.Error(), http.StatusInternalServerError)
		    	return
	  		}
			w.Header().Set("Content-Type", "application/json")
		  	w.Write(js)  	
		}
	}	
}
//
func FillProfileInfoHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method =="GET" {
		tconf := make(map[string]string)
		tconf["userid"] = r.URL.Query().Get("userid")
		tconf["email"] =  r.URL.Query().Get("email")

		tmpl := template.New("fill_profile_information.html")
		var err error
		if tmpl, err = tmpl.ParseFiles("resources/html/fill_profile_information.html"); err != nil {
			fmt.Println(err)
		}
		err = tmpl.Execute(w,tconf)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}
	}else if r.Method =="POST" {
		r.ParseForm()
		fmt.Println(r.ParseForm())
		userid 	:= r.Form["userid"][0]
		email 	:= r.Form["email"][0]
		address:= r.Form["address"][0]
		telno 	:= r.Form["telno"][0]
		fullname 	:= r.Form["fullname"][0]
    	//Database connect and update records
		affectedrow := DBModel.DBUpdateUserAccount(userid,fullname,address,email,telno)
		fmt.Println(affectedrow)
		if(affectedrow>0){
			//redirect to my profile page
			Helper.SetSession(userid, w)
			redirectURL := "/ProfilePageHandler?userid="+userid
			http.Redirect(w, r, redirectURL , 302)	
		}
			
	}
}

func ProfilePageHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method =="GET" {
		// getting html file for profile 
		ses_userID :=Helper.GetUserID(r) //get session
		if ses_userID == ``{
			redirectURL := "/"
			http.Redirect(w, r, redirectURL , 302)
		}else{
			tconf := make(map[string]string)
			tconf["userid"] = ses_userID
			tconf["is_success"] = r.URL.Query().Get("is_success")
			//database query get profile info return array of data
			UserAccountData := DBModel.DBGetUserAccount(tconf["userid"])
			//fitching record of array to map string
			tconf["username"] = UserAccountData[1]
			tconf["fullname"] = UserAccountData[4]
			tconf["address"] = UserAccountData[5]
			tconf["telephone"] = UserAccountData[6]

			tmpl := template.New("profile_page.html")
			var err error
			if tmpl, err = tmpl.ParseFiles("resources/html/profile_page.html"); err != nil {
				fmt.Println(err)
			}
			w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
			err = tmpl.Execute(w,tconf)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)

			}
		}		
	}

}

func EditProfilePageHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method =="GET" {
		tconf := make(map[string]string)
		tconf["userid"] = r.URL.Query().Get("userid")
		//database query get profile info return array of data
		UserAccountData := DBModel.DBGetUserAccount(tconf["userid"])
		//fitching record of array to map string
		tconf["username"] = UserAccountData[1]
		tconf["fullname"] = UserAccountData[4]
		tconf["address"] = UserAccountData[5]
		tconf["telephone"] = UserAccountData[6]

		tmpl := template.New("edit_profile.html")
		var err error
		if tmpl, err = tmpl.ParseFiles("resources/html/edit_profile.html"); err != nil {
			fmt.Println(err)
		}
		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
		err = tmpl.Execute(w,tconf)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}
	}else if r.Method =="POST" {
		r.ParseForm()
		userid 		:= r.Form["userid"][0]
		email 		:= r.Form["email"][0]
		address		:= r.Form["address"][0]
		telno 		:= r.Form["telno"][0]
		fullname 	:= r.Form["fullname"][0]
    	
		affectedrow := DBModel.DBUpdateUserAccount(userid,fullname,address,email,telno)
		fmt.Println(affectedrow)
		redirectURL := "/ProfilePageHandler?is_success=1&userid="+userid
		http.Redirect(w, r, redirectURL , 302)
	}

}


func LogoutHandler(w http.ResponseWriter, r *http.Request) {

	Helper.ClearSession(w)
    redirectURL := "/ProfilePageHandler"
	http.Redirect(w, r, redirectURL , 302)	

}

// Validate email to check if exist or not response via ajson data
func ValidateEmailHandler(w http.ResponseWriter, r *http.Request) {
	
	if r.Method =="POST" {
		r.ParseForm()
		email := r.Form["email"][0]
		userid := r.Form["userid"][0]
		rev_validate := r.Form["rev_validate"][0]  //to know what type of validation 1 if reverse validate 0 for not
		type Stat struct{
				Valid bool `json:"valid"`
		} 
		var ValidateData Stat
		if(len(DBModel.ValidatEmail(email,userid)) >0){
			if(rev_validate==`1`){
				ValidateData = Stat{Valid:true}
			}else{
				ValidateData = Stat{Valid:false}
			}
		}else{
			if(rev_validate==`1`){
				ValidateData = Stat{Valid:false}
			}else{
				ValidateData = Stat{Valid:true}
			}
		}

		js, err := json.Marshal(ValidateData)
		if err != nil {
	    	http.Error(w, err.Error(), http.StatusInternalServerError)
	    	return
	  		}
  		w.Header().Set("Content-Type", "application/json")
	  	w.Write(js)
	}
}



